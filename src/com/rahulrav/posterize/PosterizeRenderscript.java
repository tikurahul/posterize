package com.rahulrav.posterize;

import com.rahulrav.posterize.util.Logger;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.widget.ImageView;

public class PosterizeRenderscript extends Activity {
  @Override
  protected void onCreate(final Bundle state) {
    super.onCreate(state);
    final ImageView image = new ImageView(this);
    setContentView(image);
    drawPosterizedImage(image, R.drawable.baybridge);
  }

  private void drawPosterizedImage(final ImageView imageView, final int resoureId) {
    final Bitmap in = BitmapFactory.decodeResource(getResources(), resoureId);
    final Bitmap out = Bitmap.createBitmap(in.getWidth(), in.getHeight(), in.getConfig());
    // create context
    RenderScript rsContext = RenderScript.create(this);
    // create allocations
    Allocation inAlloc = Allocation.createFromBitmap(rsContext, in);
    Allocation outAlloc = Allocation.createTyped(rsContext, inAlloc.getType());
    // bind variables
    ScriptC_posterize posterize = new ScriptC_posterize(rsContext, getResources(), R.raw.posterize);
    posterize.set_width(in.getWidth());
    posterize.set_height(in.getHeight());
    posterize.set_in(inAlloc);
    posterize.set_out(outAlloc);
    posterize.set_script(posterize);
    // invoke
    Logger.d("Invoking Filter");
    posterize.invoke_filter();
    outAlloc.copyTo(out);
    imageView.setImageBitmap(out);
    Logger.d("Complete");
  }
  
}
