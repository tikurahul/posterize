package com.rahulrav.posterize;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;
import com.rahulrav.posterize.util.Logger;

import java.io.File;
import java.io.FileOutputStream;

public class Posterize extends Activity {

  private PosterizeTask processingTask;
  private ImageView imageView;

  static final String OUTPUT_FILE = "output.png";

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    imageView = (ImageView) findViewById(R.id.image);
    processingTask = new PosterizeTask(this);
    processingTask.execute();
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (processingTask != null && !(processingTask.getStatus() == AsyncTask.Status.FINISHED)) {
      processingTask.cancel(true);
      processingTask = null;
    }
  }

  public static class PosterizeTask extends AsyncTask<Void, Integer, Bitmap> {

    private Posterize posterize;
    private ProgressDialog progressDialog;

    public PosterizeTask(final Posterize posterize) {
      this.posterize = posterize;
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      progressDialog = ProgressDialog.show(posterize, "Posterizing", "Wait for it...", false, false);
    }

    @Override
    protected Bitmap doInBackground(final Void... nothing) {
      final BitmapFactory.Options options = new BitmapFactory.Options();
      options.inMutable = true;
      final Bitmap bitmap = BitmapFactory.decodeResource(posterize.getResources(), R.drawable.baybridge, options);
      final OilFilterEffect effect = new OilFilterEffect(bitmap);
      effect.applyInPlace();
      final Bitmap result = effect.getResult();
      Logger.d("Saving image to disk...");
      FileOutputStream outputStream = null;
      try {
        final File outputFile = new File(Environment.getExternalStorageDirectory(), OUTPUT_FILE);
        outputStream = new FileOutputStream(outputFile);
        result.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
      } catch(final Exception e) {
          Logger.e("Unable to write image", e);
      } finally {
        if (outputStream != null) {
          try {
            outputStream.close();
          } catch(final Exception ignore) {}
          outputStream = null;
        }
      }
      Logger.d("Image saved.");
      return result;
    }

    @Override
    protected void onPostExecute(final Bitmap bitmap) {
      progressDialog.dismiss();
      posterize.imageView.setImageBitmap(bitmap);
    }
  }
}