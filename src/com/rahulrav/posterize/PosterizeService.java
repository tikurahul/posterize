package com.rahulrav.posterize;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.rahulrav.posterize.util.Logger;

public class PosterizeService extends IntentService {

  /** The name of the worker thread. */
  private static final String POSTERIZE_WORKER = "PosterizeWorker";

  /** Actions. */
  public static final String ACTION_POSTERIZE = "com.rahulrav.posterize.PosterizeAction";

  /** Extras. */
  public static final String EXTRA_IMAGE = "imageToPosterize";

  public PosterizeService() {
    super(POSTERIZE_WORKER);
  }

  @Override
  protected void onHandleIntent(final Intent intent) {
    if (intent == null) {
      return;
    }

    final String action = intent.getAction();
    if (TextUtils.isEmpty(action)) {
      Logger.i("Ignoring intent, no action.");
    }

    Logger.d(String.format("Action: %s", action));
    if (ACTION_POSTERIZE.equalsIgnoreCase(action)) {
      posterizeImage(intent);
    }

  }

  /** Posterizes the image. */
  private void posterizeImage(final Intent intent) {
    final Bitmap imageToPosterize = intent.getParcelableExtra(EXTRA_IMAGE);
    if (imageToPosterize == null) {
      Logger.i("No image to posterize.");
      return;
    }

    // posterize image

  }
}
