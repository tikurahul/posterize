package com.rahulrav.posterize;

import android.graphics.Bitmap;
import android.graphics.Color;
import com.rahulrav.posterize.util.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * An in-place oil filter effect.
 */
public class OilFilterEffect {

  private final Bitmap source;

  private final Map<Integer, Integer> intensityToCount;
  private final Map<Integer, List<Integer>> intensityToPixels;

  /**
   * Some constants.
   */
  static final int WINDOW_SIZE = 7;

  public OilFilterEffect(final Bitmap source) {
    this.source = source;
    intensityToCount = new HashMap<Integer, Integer>(WINDOW_SIZE * WINDOW_SIZE);
    intensityToPixels = new HashMap<Integer, List<Integer>>(WINDOW_SIZE * WINDOW_SIZE);
  }

  public void applyInPlace() {
    if (source == null || source.getConfig() == Bitmap.Config.ALPHA_8 || !source.isMutable()) {
      Logger.i("Cannot process image.");
      return;
    }

    final Bitmap copyOfSource = Bitmap.createBitmap(source);
    final int cols = copyOfSource.getWidth();
    final int rows = copyOfSource.getHeight();
    Logger.d(String.format("Image Dimensions (%s, %s)", rows, cols));
    final int range = (WINDOW_SIZE - 1) / 2;

    for (int y = 0; y < rows; y++) {
      for (int x = 0; x < cols; x++) {
        // clear existing maps once per window
        intensityToCount.clear();
        intensityToPixels.clear();
        int maxCountIntensity = -1;
        int maxCount = -1;

        for (int yOffset = (-1 * range); yOffset <= range; yOffset++) {
          for (int xOffset = (-1 * range); xOffset <= range; xOffset++) {
            final int calcX = x + xOffset;
            final int calcY = y + yOffset;
            // check if the offsets are within the image
            if ((calcY >= 0 && calcY < rows) && (calcX >= 0 && calcX < cols)) {
              final int pixel = copyOfSource.getPixel(calcX, calcY);
              final int r = Color.red(pixel);
              final int g = Color.green(pixel);
              final int b = Color.blue(pixel);
              final int intensity = ((r + g + b) / 3);
              final List<Integer> pixels = intensityToPixels.containsKey(intensity) ? intensityToPixels.get(intensity) : new ArrayList<Integer>();
              pixels.add(pixel);
              intensityToPixels.put(intensity, pixels);
              final int count = intensityToCount.containsKey(intensity) ? (intensityToCount.get(intensity) + 1) : 1;
              intensityToCount.put(intensity, count);
              // keep track of max intensity in the window
              if (count > maxCount) {
                maxCount = count;
                maxCountIntensity = intensity;
              }
            }
          }
        }

        // processing window complete
        final List<Integer> pixels = intensityToPixels.get(maxCountIntensity);
        final int t = pixels.size();
        int sr = 0, sg = 0, sb = 0;
        for (int pixel : pixels) {
          sr += Color.red(pixel);
          sg += Color.green(pixel);
          sb += Color.blue(pixel);
        }
        final int resultPixel = Color.rgb(sr / t, sg / t, sb / t);
        source.setPixel(x, y, resultPixel);
      }
      if (y > 0 && y % 10 == 0) {
        Logger.d(String.format("Completed processing rows %s", y));
      }
    }
  }

  public Bitmap getResult() {
    return source;
  }

}
